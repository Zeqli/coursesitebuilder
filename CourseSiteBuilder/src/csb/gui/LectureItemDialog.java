/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 *
 * @author Zeqli
 */
public class LectureItemDialog extends Stage{
   // THIS IS THE OBJECT DATA BEHIND THIS UI
    Lecture lectureItem;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    TextField topicTextField;
    Label sessionLabel;
    ComboBox sessionComboBox;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPIC_PROMPT = "Topic: ";
    public static final String SESSION_PROMPT = "Number of Sessions";
    public static final String LECTURE_ITEM_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    public static final String EDIT_LECTURE_TITLE = "Edit Lecutre";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new lecture items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public LectureItemDialog(Stage primaryStage, Course course,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(LECTURE_ITEM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC 
        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lectureItem.setTopic(newValue);
        });
        
        // NOW THE SESSION COMBOBOX
        sessionLabel = new Label(SESSION_PROMPT);
        sessionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionComboBox = new ComboBox();
        sessionComboBox.setValue(1);
        ObservableList<Integer> sessionChoices = FXCollections.observableArrayList();
        for (int i = 1; i <= 3; i++) {
            sessionChoices.add(i);
        }
        sessionComboBox.setItems(sessionChoices);
        /*  addlistener(new ChangeListener<T>(){
                public void changed(ObservableValue<? extends T> observable,
                T oldValue, T newValue) {
                    lectureItem.setSessions(Integer.valueOf(sessionComboBox.getSelectionModel().getSelectedItem().toString()));}
            })
        */
        sessionComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            lectureItem.setSessions(Integer.valueOf(sessionComboBox.getSelectionModel().getSelectedItem().toString()));
        });
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(topicLabel,        0, 1, 1, 1);
        gridPane.add(topicTextField,    1, 1, 1, 1);
        gridPane.add(sessionLabel,      0, 2, 1, 1);
        gridPane.add(sessionComboBox,   1, 2, 1, 1);
        gridPane.add(completeButton,    0, 3, 1, 1);
        gridPane.add(cancelButton,      1, 3, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * Accessor method for getting the lecture item
     * 
     * @return Either 
     */
    public Lecture getLectureItem() { 
        return lectureItem;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public Lecture showAddLectureItemDialog(LocalDate initDate) {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_TITLE);
        
        // RESET THE LECTURE ITEM OBJECT WITH DEFAULT VALUES
        lectureItem = new Lecture();
        
        // LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        sessionComboBox.setValue(lectureItem.getSessions());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return lectureItem;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void showEditLectureItemDialog(Lecture itemToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_LECTURE_TITLE);
        
        // LOAD THE LECTURE ITEM INTO OUR LOCAL OBJECT
        lectureItem = new Lecture();
        lectureItem.setTopic(itemToEdit.getTopic());
        lectureItem.setSessions(itemToEdit.getSessions());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        sessionComboBox.setValue(lectureItem.getSessions());;       
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    
}
