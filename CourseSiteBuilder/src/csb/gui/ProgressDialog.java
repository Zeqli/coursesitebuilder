/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.data.Course;
import csb.data.CoursePage;
import static csb.gui.CSB_GUI.CLASS_PROGRESS_BAR;
import static csb.gui.CSB_GUI.CLASS_PROGRESS_PIE;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Zeqli
 */
public class ProgressDialog extends Stage {

    private static final String Exporting = "Exporting ";
    private static final String Completed = " Completed ";
    

    ProgressBar progress_bar;
    ProgressIndicator progress_pie;
    Label processLabel;
    ReentrantLock progressLock;

    public ProgressDialog(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        progressLock = new ReentrantLock();
        VBox box = new VBox();
        HBox toolbar = new HBox();

        processLabel = new Label();
        processLabel.setFont(new Font("Serif", 24));

        progress_bar = new ProgressBar(0);
        progress_bar.getStyleClass().add(CLASS_PROGRESS_BAR);
        progress_pie = new ProgressIndicator(0);
        progress_pie.getStyleClass().add(CLASS_PROGRESS_PIE);


        toolbar.getChildren().add(progress_bar);
        toolbar.getChildren().add(progress_pie);

        box.getChildren().add(processLabel);
        box.getChildren().add(toolbar);
        
        box.alignmentProperty().setValue(Pos.CENTER);
        toolbar.alignmentProperty().setValue(Pos.CENTER);

        Scene scene = new Scene(box, 400, 200);
        scene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(scene);

    }

    public void showProgressDialog(Course course) {
        Task<Void> task = new Task<Void>() {
            List<CoursePage> cP = course.getPages();
            double perc;
            String process;

            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    for (int i = 1; i <= cP.size(); i++) {
                        perc = (double)i / cP.size();
                        System.out.println(cP.size());
                        System.out.println(perc);
                        process = Exporting + cP.get(i - 1).toString() + Completed;

                        // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                progress_bar.setProgress(perc);
                                progress_pie.setProgress(perc);
                                processLabel.setText(process);
                            }
                        });

                        // SLEEP EACH FRAME
//                try {
                        Thread.sleep(1000);
//                } catch (InterruptedException ie) {
//                    ie.printStackTrace();
//                }
                    }
                } finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
         // THIS GETS THE THREAD ROLLING
        Thread thread = new Thread(task);
        thread.start();
        this.showAndWait();
    };
}
