package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.ScheduleItem;
import csb.data.Assignment;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.ScheduleItemDialog;
import csb.gui.LectureItemDialog;
import csb.gui.AssignmentItemDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ScheduleEditController {
    ScheduleItemDialog sid;
    LectureItemDialog lid;
    AssignmentItemDialog aid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public ScheduleEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        sid = new ScheduleItemDialog(initPrimaryStage, course, initMessageDialog);
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        aid = new AssignmentItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddScheduleItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showAddScheduleItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addScheduleItem(si);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showEditScheduleItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            itemToEdit.setDescription(si.getDescription());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToRemove) {
        if(itemToRemove == null){
            return;
        }
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeScheduleItem(itemToRemove);
        }
    }
    
    
    // THESE ARE FOR LECTURE ITEMS
    
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // GET THE LECTURE
            Lecture li = lid.getLectureItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(li);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLecutureRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Lecture li = lid.getLectureItem();
            itemToEdit.setTopic(li.getTopic());
            itemToEdit.setSessions(li.getSessions());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLecutreItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        if(itemToRemove == null){
            return;
        }
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    public void handleLectureMoveUpRequest(CSB_GUI gui, Lecture itemToMoveUp) {
        // MOVE THE SELECTED LECTURE UP BY ONE POSITION
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ObservableList<Lecture> ol= course.getLectures();
        if(!ol.isEmpty()){
            int oldPos = ol.indexOf(itemToMoveUp);
            int newPos = oldPos;
            if(oldPos > 0) {
                newPos--;
            } 
            ol.remove(itemToMoveUp);
            ol.add(newPos, itemToMoveUp);
        }
        
    }
    
    public void handleLectureMoveDownRequest(CSB_GUI gui, Lecture itemToMoveUp) {
        // MOVE THE SELECTED LECTURE UP BY ONE POSITION
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ObservableList<Lecture> ol= course.getLectures();
        if(!ol.isEmpty()){
            int oldPos = ol.indexOf(itemToMoveUp);
            int newPos = oldPos;
            if(oldPos < ol.size() - 1) {
                newPos++;
            } 
            ol.remove(itemToMoveUp);
            ol.add(newPos, itemToMoveUp);
        }
        
    }
    
    
    // THESE ARE FOR ASSIGNMENTS
    
    public void handleAddAssignmnetItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // GET THE ASSIGNMENT
            Assignment ai = aid.getAssignmentItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(ai);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditAssignmentItemRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // UPDATE THE ASSIGNMENT
            Assignment ai = aid.getAssignmentItem();
            itemToEdit.setName(ai.getName());
            itemToEdit.setTopics(ai.getTopics());
            itemToEdit.setDate(ai.getDate());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveAssignmentItemRequest(CSB_GUI gui, Assignment itemToRemove) {
        if(itemToRemove == null){
            return;
        }
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
}